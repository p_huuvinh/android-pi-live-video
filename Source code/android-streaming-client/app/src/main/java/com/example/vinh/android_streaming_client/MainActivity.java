package com.example.vinh.android_streaming_client;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;


public class MainActivity extends Activity {
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    VideoView videoView;
    EditText tf_ip;
    Button cam1;
    ProgressDialog pDialog;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
      //  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //Create a VideoView widget in the layout file
        //use setContentView method to set content of the activity to the layout file which contains videoView
        this.setContentView(R.layout.activity_main);
        initPermission();
       // pDialog = new ProgressDialog(MainActivity.this);
        cam1=(Button) findViewById(R.id.btn);
        videoView = (VideoView) findViewById(R.id.video);
        cam1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                // Set progressbar title
//                pDialog.setTitle("Android Video Streaming");
//                // Set progressbar message
//                pDialog.setMessage("Buffering...");
//                pDialog.setIndeterminate(false);
//                pDialog.setCancelable(false);
//                // Show progressbar
//                pDialog.show();
                //    playStream();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("rtsp://192.168.2.106:8554/"));
                intent.setPackage("org.videolan.vlc");
                startActivity(intent);
            }
        });
    }
    private void playStream(){
        String ip, uri;
        tf_ip = (EditText) findViewById(R.id.tf_tf);

        ip="rtsp://"+tf_ip.getText()+":8554/ ";
        uri = "rtsp://192.168.43.156:8554/";
         MediaController mc = new MediaController(this);
        Uri src = Uri.parse(ip);
        mc.setAnchorView(videoView);
        videoView.setVideoURI(src);
        videoView.setMediaController(mc);
        videoView.requestFocus();
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
//                pDialog.dismiss();
                mediaPlayer.start();
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {
                        mediaPlayer.start();
                    }
                });
            }
        });

    }
    private void PlayVideo()
    {
        try
        {
           // getWindow().setFormat(PixelFormat.TRANSLUCENT);
            MediaController mediaController = new MediaController(MainActivity.this);
            mediaController.setAnchorView(videoView);

            Uri video = Uri.parse("rtsp://192.168.43.156:8554/" );
            videoView.setMediaController(mediaController);
            videoView.setVideoURI(video);
            videoView.requestFocus();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener()
            {
                public void onPrepared(MediaPlayer mp)
                {
                    videoView.start();
                }
            });


        }
        catch(Exception e)
        {
            System.out.println("Video Play Error :"+e.toString());
            finish();
        }

    }

    // PERMISSION
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(MainActivity.this, "Permision internet is Granted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "Permision internet is Denied", Toast.LENGTH_SHORT).show();

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    public void initPermission(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {

                //Permisson don't granted
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.INTERNET)) {
                    Toast.makeText(MainActivity.this, "Permission isn't granted ", Toast.LENGTH_SHORT).show();
                }
                // Permisson don't granted and dont show dialog again.
                else {
                    Toast.makeText(MainActivity.this, "Permisson don't granted and dont show dialog again ", Toast.LENGTH_SHORT).show();
                }
                //Register permission
                requestPermissions(new String[]{Manifest.permission.INTERNET}, 1);

            }
        }
    }
}

